﻿
DisplayName "Pyramid"



EVENT   1
 PAGE   1
  0()



EVENT   2
 PAGE   1
  0()



EVENT   3
 PAGE   1
  0()



EVENT   4
 PAGE   1
  0()



EVENT   5
 PAGE   1
  0()



EVENT   6
 PAGE   1
  0()



EVENT   7
 PAGE   1
  0()



EVENT   8
 PAGE   1
  0()



EVENT   9
 PAGE   1
  0()



EVENT   10
 PAGE   1
  0()



EVENT   11
 PAGE   1
  0()



EVENT   12
 PAGE   1
  0()



EVENT   13
 PAGE   1
  0()



EVENT   14
 PAGE   1
  0()



EVENT   15
 PAGE   1
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x09,0x4d,0x6f,0x76,0x65,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  TeleportPlayer(0,2,77,296,0,0)
  0()



EVENT   16
 PAGE   1
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x09,0x4d,0x6f,0x76,0x65,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  TeleportPlayer(0,2,77,296,0,0)
  0()



EVENT   17
 PAGE   1
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x09,0x4d,0x6f,0x76,0x65,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  TeleportPlayer(0,2,77,296,0,0)
  0()



EVENT   18
 PAGE   1
  ChangeSwitch(100,100,0)
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x09,0x4d,0x6f,0x76,0x65,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  TeleportPlayer(0,114,30,55,0,0)
  0()



EVENT   19
 PAGE   1
  If(2,binary"A",1)
   ChangeSelfSwitch(binary"A",0)
   ShowMessageFace("sasori_fc1",0,0,2,1)
   ShowMessage("\\n<Scorpion Girl>You guys, did you come here to undertake the Trial of Dragon Seal...?")
   ShowChoices(strings("That's right","We came for other reason","Just passing by"),0)
   IfPlayerPicksChoice(0,null)
    ShowMessageFace("sasori_fc1",0,0,2,2)
    ShowMessage("\\n<Scorpion Girl>Then expect fierce battles inside the Pyramid. Mummy Girl's special ability is especially nasty...")
    0()
   IfPlayerPicksChoice(1,null)
    ShowMessageFace("sasori_fc1",0,0,2,3)
    ShowMessage("\\n<Scorpion Girl>Oh my, is that so... but you're going to enter the Pyramid aren't you? The Mummy Girl who appears inside has an especially nasty ability...")
    0()
   IfPlayerPicksChoice(2,null)
    ShowMessageFace("sasori_fc1",0,0,2,4)
    ShowMessage("\\n<Scorpion Girl>Oh my, is that so... but since you're here, how about going inside? The Mummy Girl who appears inside has an especially nasty ability...")
    0()
   404()
   0()
  EndIf()
  ShowMessageFace("sasori_fc1",0,0,2,5)
  ShowMessage("\\n<Scorpion Girl>When you receive Mummy Girl's curse attack your body will turn into stone. You won't be able to move and it won't disappear until the battle ends.")
  ShowMessageFace("sasori_fc1",0,0,2,6)
  ShowMessage("\\n<Scorpion Girl>But! This Gold Needle can release petrification! Buy plenty of them here so that you don't end up crying later!")
  Shop(0,20,0,0,true)
  0()
